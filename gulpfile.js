var theme = 'blank';
var dir = {
  build: 'wp-content/themes/'+theme+'/build/',
  template: 'wp-content/themes/'+theme+'/'
};

// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var eslint = require('gulp-eslint');
var scsslint = require('gulp-scss-lint');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var addsrc = require('gulp-add-src');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var livereload = require('gulp-livereload');
var plumber = require('gulp-plumber');
var watch = require('gulp-watch');


// Lint Task
gulp.task('lint', ['eslint', 'scss-lint']);

// es-lint
gulp.task('eslint', ['eslint-basic', 'eslint-load-config-shorthand']);

gulp.task('eslint-basic', function() {
  return gulp.src(['!'+dir.build+'js/front/lib/**/*.js', dir.build+'js/front/**/*.js'])
    .pipe(eslint())
    .pipe(eslint.format());
});
gulp.task('eslint-load-config-shorthand', function() {
  return gulp.src(['!'+dir.build+'js/front/lib/**/*.js', dir.build+'js/front/**/*.js'])
    .pipe(eslint( 'gulp-eslint-config.json' ))
    .pipe(eslint.format());
});

gulp.task('scss-lint', function (){
  return gulp.src( dir.build+'sass/**/*.scss' )
    .pipe(scsslint({
      'config': 'gulp-scss-lint-config.yml'
    }));
});


// Compile Our Sass
var sassOptions = { errLogToConsole: true };
var autoprefixerOptions = { browsers: ['last 2 versions', '> 5%', 'Firefox ESR'] };
gulp.task('sass', function () {

  gulp.src(dir.build+'sass/**/*.scss')
    .pipe(plumber())
    
    .pipe(sourcemaps.init( {loadMaps: false} ))
    .pipe(sass(sassOptions).on('error', sass.logError))
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(rename('sass.css'))
    .pipe(sourcemaps.write())

    .pipe(addsrc(['!'+dir.build+'css/theme-header.css', dir.build+'css/**/*.css']))
    .pipe(sourcemaps.init( {loadMaps: false} ))
    .pipe(concat('css.css'))
    .pipe(sourcemaps.write())

    .pipe(addsrc(dir.build+'css/theme-header.css'))
    .pipe(sourcemaps.init( {loadMaps: false} ))
    .pipe(concat('style.css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(dir.template+'.'))
    .pipe(livereload());

});

// Concatenate JS
gulp.task('scripts', function() {
  gulp.src( [dir.build+'js/front/lib/jquery.js', dir.build+'js/front/lib/**/*.js', dir.build+'js/front/**/*.js'] )
    .pipe(concat('front.min.js'))
    //.pipe(uglify())
    .pipe(gulp.dest( dir.template+'js' ))

  gulp.src( dir.build+'js/admin/wp-admin.js' )
    .pipe(rename('admin.min.js'))
    //.pipe(uglify())
    .pipe(gulp.dest( dir.template+'js' ));
});

gulp.task('php', function() {
  return gulp.src( 'index.php' )
    .pipe( livereload() );
});

// Watch Files For Changes
gulp.task('watch', function() {
  livereload.listen();
  gulp.watch( dir.build+'js/**/*.js', ['scripts', 'eslint'] );
  gulp.watch( [dir.build+'css/**/*.css', dir.build+'sass/**/*.scss'], ['sass', 'scss-lint'] );
  gulp.watch( dir.template + '**/*.php', ['php'] );

  gulp.src(dir.template+'js/**/*.js')
    .pipe(watch(dir.template+'js/**/*.js'))
    .pipe(livereload());
});

gulp.task('uglify', function() {
  gulp.src( dir.template+'js/front.min.js' )
    .pipe(uglify())
    .pipe(gulp.dest(dir.template+'js'))

  gulp.src( dir.template+'js/admin.min.js' )
    .pipe(uglify())
    .pipe(gulp.dest(dir.template+'js'));
});

gulp.task('minifyCss', function() {
  gulp.src( dir.template+'style.css' )
    .pipe(minifyCss())
    .pipe(rename( 'style.min.css' ))
    .pipe(gulp.dest(dir.template+'.'))
    .pipe(addsrc( dir.build+'css/theme-header.css' ))
    .pipe(concat( 'style.min.css' ))
    .pipe(gulp.dest(dir.template+'.'));
});

// Default Task
gulp.task('default', ['lint', 'sass', 'scripts', 'watch']);
gulp.task('compress', ['uglify', 'minifyCss']);

