<?php

/**
 * The base configurations of the WordPress.
 *
 **
 * Modified for github deployment for Pace Creative
 *
 * Make changes as necessary to the local environment block
 *
 * DO NOT COMMIT THIS FILE
 **
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

 if ($_SERVER['REMOTE_ADDR']=='127.0.0.1' || $_SERVER['REMOTE_ADDR']=='::1') {
    define('WP_ENV', 'local');
} else {
    define('WP_ENV', 'staging');
}

define('WP_DEFAULT_THEME', 'blank');

// define custom values for different environments
if (WP_ENV == 'local') {

    // these directories should be based on a default clone from github
    // only modify if cloned to a folder different than the repository name
    define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/blank/wordpress');
    define('WP_HOME',    'http://' . $_SERVER['SERVER_NAME'] . '/blank/');

    define('WP_CONTENT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/blank/wp-content');
    define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/blank/wp-content');

    // ** MySQL settings - You can get this info from your web host ** //
    define('DB_NAME', 'blank');
    define('DB_USER', 'root');
    define('DB_PASSWORD', '');
    define('DB_HOST', 'localhost');

} else {
    // live directories
    define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/wordpress');
    define('WP_HOME',    'http://' . $_SERVER['SERVER_NAME'] );

    define('WP_CONTENT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/wp-content');
    define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp-content');

    define('DB_NAME', 'pace_blank');
    define('DB_USER', 'pace_blank');
    define('DB_PASSWORD', '');
    define('DB_HOST', 'localhost');
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'D|M``|uV56qQx-KAZtVr>d<oH`4[Upz:pVn!*L}Bf0U[R-:(cUw(z&d&1{8NhoJ+');
define('SECURE_AUTH_KEY',  'IDP_@#dV6XjbBVJ,0[)_T(PpK*sp8z8b|Ops@@O:!cLgpy=^4kH+ql]FZ$dKR1]7');
define('LOGGED_IN_KEY',    '^Yt8*TMPRkv83ahE_:53dqB5ah {x/wG{?-TnIDfK.]X-_kzScaMu:|HOEd ->20');
define('NONCE_KEY',        ']KTZ.TtYmN^KJZwrNf_i+ -{$Qw)9p[6);$Wu#-W#)5bw610c;@B7]=<{@`,L59;');
define('AUTH_SALT',        '*Tt-m(-:nw%Z);q];},9F%,?9DGaL|Kw|03Ij+@NHSP73)H&GCC_<=1A_ed8ZQTS');
define('SECURE_AUTH_SALT', 'CDqSk/o!$%(+1&aO ((wA)0XYiYXScXln&Ra|C8JI-3Tes2(|*R7*Cp-BP+!}d7w');
define('LOGGED_IN_SALT',   '~@lU#@^|i3zXC=9l$B}AW1.fDS0.$/.)Su,U>XT$7yc<}^^Q49a@s6BkHI^Pj,O+');
define('NONCE_SALT',       'eydD FM<Xpqyp-|R(NPuazOPzj=X11-4Ic1ol}in1iTTlu>>gv,3;#c<[HX;s,7+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
