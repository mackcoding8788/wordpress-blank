/**
 * Name: preloadImg
 * Version: 0.3
 **/

(function($) {

  'use strict';

  // preload images
  $.fn.preloadImg = function( options ) {

    // set defaults
    var settings = $.extend({
      path: ''
    }, options );

    return this.each(function() {

      $('<img/>')[0].src = settings.path + this;

    });

  };

}(jQuery));