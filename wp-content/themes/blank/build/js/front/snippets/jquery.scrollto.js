/**
 * Name: scrollTo
 * Version: 0.1
 **/

(function($) {

  'use strict';

  // match all item heights to biggest element
  $.fn.scrollTo = function() {

    // use data-href to change location
    return $(this).click(function() {

      var $target = $($(this).data('target'));

      var top = $target.offset().top;

      $('html, body').animate({
        scrollTop: top+'px'
      }, 800);

    });

  };

}(jQuery));