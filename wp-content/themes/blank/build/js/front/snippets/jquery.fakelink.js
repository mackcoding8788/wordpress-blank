/**
 * Name: fakeLink
 * Version: 0.3
 *
 * 0.3
 * -better implementation of link role
 * -add option for inner link functionality
 *
 * 0.2
 * -add button role for accessibility
 **/

(function($) {

  'use strict';

  $.fn.fakeLink = function(options) {

    // set defaults
    var settings = $.extend({
      allow_inner: true
    }, options);

    var $fakelink = $(this);

    // help out assistive technology
    $fakelink.attr('role', 'link');
    $fakelink.attr('tabindex', 0);
    $fakelink.attr('href', $fakelink.data('href'));
    $fakelink.removeAttr('data-href');

    // follow href on click
    $fakelink.click(function() {
      var href = $fakelink.attr('href');
      window.location = href;
    });

    // simulate click on enter
    $fakelink.keypress( function(e) {
      if( e.which == 13 ) {
        this.click();
      }
    });

    if (settings.allow_inner) {

      // allow links inside fake links to still fuction
      $fakelink.find('a').click(function(e) {
        e.stopPropagation();
      });

    }

    return this;

  };

}(jQuery));
