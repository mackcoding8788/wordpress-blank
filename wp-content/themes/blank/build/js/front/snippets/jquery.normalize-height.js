/**
 * Name: normalizeHeight
 * Version: 0.5
 **/
(function($) {

  'use strict';

  // match all item heights to biggest element
  $.fn.normalizeHeight = function( options ) {

    // set defaults
    var settings = $.extend({
      by_row: false
    }, options );

    this.css('height', 'auto');

    if( settings.by_row ) {

      var row_top = 0;
      var cur_row = [];

      // loop to find heights for each row
      this.each(function() {

        // get top of element
        var ptop = $(this).getTop();

        // check if we've started a new row
        if( ptop != row_top ) {

          // normalize current row
          if( cur_row.length > 0 ) {
            $(cur_row).normalizeHeight();
          }

          // reflow
          ptop = $(this).getTop();

          // reset row
          row_top = ptop;
          cur_row = [];
        }

        // add current element to row
        cur_row.push(this);

      });

      // once more for last row
      $(cur_row).normalizeHeight();

      return this;
    }

    // check if we are sizing by row or not
    if( settings.by_row === false ) {

      // set all heights to max
      return this.css('height', $(this).getMaxHeight());

    }

  };

  $.fn.getMaxHeight = function() {

    var max_height = 0;

    // loop to find heights for each row
    this.each(function() {

      // height of item
      var cur_height = $(this).outerHeight();

      // set total max height
      if(cur_height > max_height) {
        max_height = cur_height;
      }

    });

    return max_height;

  };

  $.fn.getTop = function() {

    // find vertical position
    var position = $(this).offset();
    return Math.floor(position.top);

  };

}(jQuery));