<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <!-- page title -->
  <title><?php wp_title(''); ?></title>

  <?php wp_head(); ?>

  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,700,700italic' rel='stylesheet' type='text/css'>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>
<span id="top"></span>
<header id="header" class="noprint">


<?php
/*
  wp_nav_menu( array(
      'theme_location'  => 'main_nav',
      'container'       => false,
      'menu_class'      => 'main-navigation',//  navbar-right
      'walker'          => new Pace_Nav_Walker(),
    )
  );
*/
?>

</header>

<section id="body">