<?php

/***
  * Hijack gallery shortcode
  * Check out wp-includes/media.php for parameters
  */

add_shortcode('gallery', 'pace_gallery_shortcode');
function pace_gallery_shortcode( $attr ) {
  $post = get_post();

  static $instance = 0;
  $instance++;

  if ( ! empty( $attr['ids'] ) ) {
    // 'ids' is explicitly ordered, unless you specify otherwise.
    if ( empty( $attr['orderby'] ) )
      $attr['orderby'] = 'post__in';
    $attr['include'] = $attr['ids'];
  }

  /**
   * Filter the default gallery shortcode output.
   *
   * If the filtered output isn't empty, it will be used instead of generating
   * the default gallery template.
   *
   * @since 2.5.0
   *
   * @see gallery_shortcode()
   *
   * @param string $output The gallery output. Default empty.
   * @param array  $attr   Attributes of the gallery shortcode.
   */
  $output = apply_filters( 'post_gallery', '', $attr );
  if ( $output != '' )
    return $output;

  // We're trusting author input, so let's at least make sure it looks like a valid orderby statement
  if ( isset( $attr['orderby'] ) ) {
    $attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
    if ( !$attr['orderby'] )
      unset( $attr['orderby'] );
  }

  $html5 = current_theme_supports( 'html5', 'gallery' );
  extract(shortcode_atts(array(
    'order'      => 'ASC',
    'orderby'    => 'menu_order ID',
    'id'         => $post ? $post->ID : 0,
    'itemtag'    => $html5 ? 'figure'     : 'dl',
    'icontag'    => $html5 ? 'div'        : 'dt',
    'captiontag' => $html5 ? 'figcaption' : 'dd',
    'columns'    => 3,
    'size'       => 'gallery-thumb',
    'include'    => '',
    'exclude'    => '',
    'link'       => ''
  ), $attr, 'gallery'));

  $id = intval($id);
  if ( 'RAND' == $order )
    $orderby = 'none';

  if ( !empty($include) ) {
    $_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

    $attachments = array();
    foreach ( $_attachments as $key => $val ) {
      $attachments[$val->ID] = $_attachments[$key];
    }
  } elseif ( !empty($exclude) ) {
    $attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
  } else {
    $attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
  }

  if ( empty($attachments) )
    return '';

  if ( is_feed() ) {
    $output = "\n";
    foreach ( $attachments as $att_id => $attachment )
      $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
    return $output;
  }

  $itemtag = tag_escape($itemtag);
  $captiontag = tag_escape($captiontag);
  $icontag = tag_escape($icontag);
  $valid_tags = wp_kses_allowed_html( 'post' );
  if ( ! isset( $valid_tags[ $itemtag ] ) )
    $itemtag = 'dl';
  if ( ! isset( $valid_tags[ $captiontag ] ) )
    $captiontag = 'dd';
  if ( ! isset( $valid_tags[ $icontag ] ) )
    $icontag = 'dt';

  $columns = intval($columns);
  $itemwidth = $columns > 0 ? floor(100/$columns) : 100;
  $float = is_rtl() ? 'right' : 'left';

  $selector = "gallery-{$instance}";

  $gallery_style = $gallery_div = '';

  /**
   * Filter whether to print default gallery styles.
   *
   * @since 3.1.0
   *
   * @param bool $print Whether to print default gallery styles.
   *                    Defaults to false if the theme supports HTML5 galleries.
   *                    Otherwise, defaults to true.
   */
  if ( apply_filters( 'use_default_gallery_style', ! $html5 ) ) {
    $gallery_style = "
    <style type='text/css'>
      #{$selector} {
        margin: auto;
      }
      #{$selector} .gallery-item {
        float: {$float};
        text-align: center;
        width: {$itemwidth}%;
        margin-bottom: 50px;
      }
      .gallery-item__info {
        padding-top: 15px;
        padding-bottom: 15px;
        background: #fff;
        border-radius: 0 0 5px 5px;
        width: 95%;
        margin-left: 2.5%;
      }
      .gallery-item__title {
        margin-left: 15px;
        display: inline-block;
        width: 50%;
        font-weight: 300;
        font-size: 16px;
        float: left;
        text-align: left;
      }
      .gallery-item__links {
        margin: 0;
        margin-right: 15px;
        padding: 0;
        float: right;
      }
      .gallery-item__links > li {
        float: none;
        list-style: none;
        text-align: left;
        color: #F79420;
      }
      .gallery-item__links > li + li {
        margin-top: 5px;
      }

      .gallery-item__links > li > a {
        font-weight: 300;
      }
      #{$selector} img {
        max-width: 100%;
        margin: 0 auto;
        border-radius: 5px 5px 0 0;
      }
      #{$selector} img.big-img {
        width: auto;
        height: auto;
        max-width: 100%;
        max-height: 185px;
        margin-bottom: 10px;
      }
      #{$selector} .gallery-caption {
        margin-left: 0;
      }
      #{$selector} .gallery-link {
        display: inline-block;
        width: 95%;
        height: 95%;
      }
      @media (min-width: 768px) {
        #{$selector} img.big-img {
          max-height: 461px;
        }
      }
      @media (min-width: 992px) {
        #{$selector} img.big-img {
          max-height: 327px;
        }
      }
      @media (min-width: 1200px) {
        #{$selector} img.big-img {
          max-height: 393px;
        }
      }
      /* see gallery_shortcode() in wp-includes/media.php */
    </style>\n\t\t";
  }

  $size_class = sanitize_html_class( $size );
  $gallery_div = "<div id='$selector' class='gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";

  /**
   * Filter the default gallery shortcode CSS styles.
   *
   * @since 2.5.0
   *
   * @param string $gallery_style Default gallery shortcode CSS styles.
   * @param string $gallery_div   Opening HTML div container for the gallery shortcode output.
   */
  $output = apply_filters( 'gallery_style', $gallery_style . $gallery_div );

  $i = 0;
  foreach ( $attachments as $id => $attachment ) {

    if ( ! empty( $link ) && 'file' === $link ) {
      $src = wp_get_attachment_image_src( $id, 'large' );
      $attr = wp_prepare_attachment_for_js($id);
      $image_output = wp_get_attachment_image( $id, $size, false );
      $image_output = '<a href="'.$src[0].'" data-title="'.$attr['description'].'" class="gallery-link" data-lightbox="gallery" target="_blank">'.$image_output.'</a>';
    } elseif ( ! empty( $link ) && 'none' === $link ) {
      $image_output = wp_get_attachment_image( $id, $size, false );
    } else {
      $image_output = wp_get_attachment_link( $id, $size, false, false );
    }

    $image_meta  = wp_get_attachment_metadata( $id );

    // if( $i == 0 ) {
    //   $output .= '<div style="width:96%; background: #f2ebd8;">'.wp_get_attachment_image( $id, 'full', false, array( 'class' => 'img-responsive big-img' ) ).'</div>';
    // }

    $orientation = '';
    if ( isset( $image_meta['height'], $image_meta['width'] ) )
      $orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';

    $output .= "<{$itemtag} class='gallery-item'>";
    $output .= "
      <{$icontag} class='gallery-icon {$orientation}'>
        $image_output
        <div class='gallery-item__info clearfix'>

          <span class='gallery-item__title'>".$attr['title']."</span>
          <ul class='gallery-item__links'>
            <li><i class='fa fa-download'></i> <a href='".$src[0]."' target='_blank'>Download</a></li>
            <!--<li><i class='fa fa-share-alt'></i> <a href='#'>Share</a></li>-->
          </ul>

        </div>
      </{$icontag}>";
    if ( $captiontag && trim($attachment->post_excerpt) ) {
      $output .= "
        <{$captiontag} class='wp-caption-text gallery-caption'>
        " . wptexturize($attachment->post_excerpt) . "
        </{$captiontag}>";
    }
    $output .= "</{$itemtag}>";
    if ( ! $html5 && $columns > 0 && ++$i % $columns == 0 ) {
      $output .= '<br style="clear: both" />';
    }
  }

  if ( ! $html5 && $columns > 0 && $i % $columns !== 0 ) {
    $output .= "
      <br style='clear: both' />";
  }

  $output .= "
    </div>\n";

  return $output;
}