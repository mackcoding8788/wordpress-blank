<?php

/**
 * Includes
**/
include 'inc/template-tags.php';
include 'inc/nav-walker.php';

// theme functionality
register_nav_menus( array(
  'main_nav'=>'Primary Navigation',
));
add_theme_support( 'post-thumbnails' );

// add_image_size( 'gallery-thumb', 300, 200, true );

// var dump with auto <pre>
function pvar_dump($a) { echo '<pre>'; var_dump($a); echo '</pre>'; }

function pace_add_query_vars($aVars) {

  // $aVars[] = 'curyear';

  return $aVars;
}
add_filter('query_vars', 'pace_add_query_vars');

function pace_add_rewrite_rules($aRules) {
  $aNewRules = array(
    // 'investors/presentations/([^/]+)/?$' => 'index.php?pagename=investors/presentations&curyear=$matches[1]',
   );
  $aRules = $aNewRules + $aRules;
  return $aRules;
}
add_filter('rewrite_rules_array', 'pace_add_rewrite_rules');


// init hook
add_action( 'init', 'pace_init' );
function pace_init() {

  add_editor_style();
  add_editor_style('http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');

  // example custom post type and taxonomy

  /*
  // add post type
  $labels = array(
    'name'               => _x( 'Properties', 'post type general name', 'pace' ),
    'singular_name'      => _x( 'Property', 'post type singular name', 'pace' ),
    'menu_name'          => _x( 'Properties', 'admin menu', 'pace' ),
    'name_admin_bar'     => _x( 'Property', 'add new on admin bar', 'pace' ),
    'add_new'            => _x( 'Add New', 'property', 'pace' ),
    'add_new_item'       => __( 'Add New Property', 'pace' ),
    'new_item'           => __( 'New Property', 'pace' ),
    'edit_item'          => __( 'Edit Property', 'pace' ),
    'view_item'          => __( 'View Property', 'pace' ),
    'all_items'          => __( 'All Properties', 'pace' ),
    'search_items'       => __( 'Search Properties', 'pace' ),
    'parent_item_colon'  => __( 'Parent Property:', 'pace' ),
    'not_found'          => __( 'No properties found.', 'pace' ),
    'not_found_in_trash' => __( 'No properties found in Trash.', 'pace' )
  );

  $args = array(
    'labels' => $labels,
    'supports' => array(
      'title',
      'editor',
      'thumbnail',
    ),
    'public' => true,
    'has_archive' => true,
    'menu_icon' => 'dashicons-admin-home',
    'menu_position' => 50,
    );

  register_post_type( 'property', $args );
  add_post_type_support( 'property', 'page-attributes' );


  $labels = array(
    'name'              => _x( 'Property Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Property Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Property Categories' ),
    'all_items'         => __( 'All Property Categories' ),
    'parent_item'       => __( 'Parent Property Category' ),
    'parent_item_colon' => __( 'Parent Property Category:' ),
    'edit_item'         => __( 'Edit Property Category' ),
    'update_item'       => __( 'Update Property Category' ),
    'add_new_item'      => __( 'Add New Property Category' ),
    'new_item_name'     => __( 'New Property Category Name' ),
    'menu_name'         => __( 'Property Category' ),
  );

  register_taxonomy(
    'property_category',
    'property',
    array(
      'labels' => $labels,
      'hierarchical' => true,
    )
  );
  */

}
// admin init hook
add_action( 'admin_init', 'pace_admin_init' );
function pace_admin_init() {

}

function remove_editor() {
  if (isset($_GET['post'])) {
    $id = $_GET['post'];
    $template = get_post_meta($id, '_wp_page_template', true);
/*
    if($template == 'page-investors.php'){
      remove_post_type_support( 'page', 'editor' );
    }
*/
  }
}

function search_distinct() {
  return "DISTINCT";
}
add_filter('posts_distinct', 'search_distinct');

/**
 * Shortcodes
 */

// include scripts
function pace_scripts() {

  wp_enqueue_style( 'bootstrap-style', home_url().'/bootstrap/css/bootstrap.css', array(), '3.3.2' );
  wp_enqueue_style( 'font-awesome-style', home_url().'/font-awesome-4.4.0/css/font-awesome.min.css', array(), '4.4.0' );
  wp_enqueue_style( 'theme-style', get_stylesheet_uri(), array(), '1.0' );
  //wp_enqueue_style( 'theme-style-min', get_stylesheet_directory_uri().'/style.min.css', array(), '1.0' );
  wp_enqueue_style( 'theme-print-style', get_stylesheet_directory_uri().'/print.css', array(), false, 'print' );
  
  // JQuery and other libraries
  wp_enqueue_script( 'front-scripts', get_stylesheet_directory_uri().'/js/front.min.js',array(),'1',true);
  wp_enqueue_script( 'bootstrap-script', home_url().'/bootstrap/js/bootstrap.min.js',array(),'3.3.2',true);
  
  $localize_vals = array(
    'WP_HOME' => WP_HOME,
    'curpage' => get_the_permalink(),
    'time' => time(),
    'stylesheet_url' => get_stylesheet_directory_uri(),
    'ajax_url' => admin_url( 'admin-ajax.php' ),
  );
  wp_localize_script( 'front-scripts', 'php_vals', $localize_vals, 'pace');

}
add_action( 'wp_enqueue_scripts', 'pace_scripts' );


//** Neuter Admin Section **//

// Home BOXES control
add_action( 'admin_menu', 'register_pace_admin', 9999 );
function register_pace_admin(){

  // remove items not in use in admin menu
  global $menu;

  // unused sections
  $restricted = array(
    __('menu-comments'),
    __('menu-posts'),
  );

  // restrict by role
  $current_user = wp_get_current_user();

  if(!in_array( 'administrator', $current_user->roles )) {
    // $restricted[] = __('menu-plugins');
    $restricted[] = __('menu-users');
    $restricted[] = __('menu-tools');
    // $restricted[] = __('menu-settings');
    // $restricted[] = __('menu-appearance');
    $restricted[] = __('toplevel_page_wpseo_dashboard');
    $restricted[] = __('toplevel_page_wpcf7');

    // remove_submenu_page('index.php', 'update-core.php');
    // remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=category');
    // remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');
    // remove_submenu_page('edit.php?post_type=page', 'post-new.php?post_type=page');

  }

  // add a separator at the end
  $menu[] = array(
    0 =>  '',
    1 =>  'read',
    2 =>  'separator',
    3 =>  '',
    4 =>  'wp-menu-separator'
  );

  end ($menu);
  while (prev($menu)){
    $value = isset($menu[key($menu)][5]) ? $menu[key($menu)][5] : false;

    if(in_array($value, $restricted)){unset($menu[key($menu)]);}
  }
}

// customize admin bar
add_action( 'wp_before_admin_bar_render', 'modify_admin_bar' );


function modify_admin_bar(){

  global $wp_admin_bar;

  // general restrictions
  $wp_admin_bar->remove_menu('comments');

  // restrict by role
  $current_user = wp_get_current_user();

  if(!in_array( 'administrator', $current_user->roles )) {
    $wp_admin_bar->remove_menu('wp-logo');
    // $wp_admin_bar->remove_menu('new-content');
    $wp_admin_bar->remove_menu('wpseo-menu');
  }

}
//** End Neuter Admin Section **//

function the_excerpt_max_charlength($charlength) {
  $excerpt = get_the_excerpt();
  $charlength++;

  if ( mb_strlen( $excerpt ) > $charlength ) {
    $subex = mb_substr( $excerpt, 0, $charlength - 5 );
    $exwords = explode( ' ', $subex );
    $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
    if ( $excut < 0 ) {
      echo mb_substr( $subex, 0, $excut );
    } else {
      echo $subex;
    }
    echo '[...]';
  } else {
    echo $excerpt;
  }
}

add_filter( 'nav_menu_link_attributes', 'pace_menu_link_attributes', 10, 3 );
function pace_menu_link_attributes( $atts, $item, $args ) {

  if( $item->menu_item_parent == 0 ) {

    $_tmp = explode( ' ', $args->menu_class );

    foreach( $_tmp as &$class ) {

      $class .= '__link';

    }

    // $atts['class'] = $args->menu_class . '__link';
    $atts['class'] = implode( ' ', $_tmp );

  }


  return $atts;
}

add_filter( 'nav_menu_css_class', 'pace_menu_css_class', 10, 3 );
function pace_menu_css_class( $classes, $item, $args ) {

  if( $item->menu_item_parent == 0 ) {

    $_tmp = explode( ' ', $args->menu_class );

    foreach( $_tmp as &$class ) {

      $classes[] =  $class . '__item';

    }

  }

  return $classes;
}

function pace_custom_admin_js() {
    $url = get_bloginfo('template_directory') . '/js/admin.min.js';
    echo '"<script type="text/javascript" src="'. $url . '"></script>"';
}
add_action('admin_footer', 'pace_custom_admin_js');

// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {

  // Define the style_formats array
  $style_formats = array(
    // Each array child is a format with it's own settings
    // array(
    //   'title' => 'Button Link - Plain',
    //   'selector' => 'a',
    //   'classes' => 'btn btn-primary',
    // ),

  );
  // Insert the array, JSON ENCODED, into 'style_formats'
  $init_array['style_formats'] = json_encode( $style_formats );

  return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

function check_cache_file($file) {

  // check for existing file
  if( file_exists( $file ) ) {

    // find out how old the file is
    $filetime = filemtime($file);
    $timediff = time() - $filetime;

    // check if file has timed out
    if( $timediff < CACHE_TIMEOUT ) {
      return file_get_contents( $file );
    }

  }

  return false;
}

function checkdir($dir) {
  if( is_dir( $dir ) || mkdir( $dir )) {
    return $dir;
  } else {
    return false;
  }
}

function human_filesize($bytes, $decimals = 2) {
  $sz = 'BKMGTP';
  $factor = floor((strlen($bytes) - 1) / 3);
  return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
}



// Change what's hidden by default
add_filter('hidden_meta_boxes', 'hide_meta_lock', 10, 2);
function hide_meta_lock($hidden, $screen) {

  // hiden yoast SEO box by default
  $hidden[] = 'wpseo_meta';

  return $hidden;
}


// Remove header junk
remove_action( 'wp_head', 'wp_generator');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'rsd_link');
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );