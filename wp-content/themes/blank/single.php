<?php get_header(); ?>
<?php
  if( have_posts() ) {
    the_post();
  }
?>
<div class="page-section page-section--wide">
  <div class="container">

    <div class="row">

      <div class="col-xs-12 col-sm-7">
        <div class="page-section__content">
          <h2><?php the_title(); ?></h2>
          <?php the_content(); ?>
        </div>
      </div>

    </div>

  </div>
</div>

<?php get_footer(); ?>