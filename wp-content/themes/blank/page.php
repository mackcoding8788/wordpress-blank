<?php get_header(); ?>

<div class="container">

  <div class="row">
    <div class="col-xs-12 col-lg-10 col-lg-offset-1">

      <?php if ( have_posts() ) the_post(); ?>

      <h1 class="page-title"><?php the_title(); ?></h1>

      <?php the_content(); ?>

   </div><!-- .col-xs-12 .col-lg-10 .col-lg-offset-1 -->
 </div><!-- .row -->

</div><!-- .container .content -->

<?php get_footer(); ?>